# README #

#ArmyOfOnes

You plan on traveling to four countries carrying a stack of 1 US dollar bills. You will need to be converting these bills to local currency quite often, and you want to make sure you don’t get swindled along the way. Write an app that will display the current exchange rate for any number of dollar bills, represented by an integer value, into the following currencies: GBP, EUR, JPY, BRL.

## How do I get set up? ##

### Summary of set up ###

Clone, do a Pod Install and run with Xcode 7 and iOS 9.3

### Dependencies ###

[Cocoapods](https://cocoapods.org/)

### How to run tests ###

Test are built with [Quick/Nimble](https://github.com/Quick/Quick) testing framework
Do Cmd + U to start running the tests

## Libraries used ##

* [Then](https://github.com/devxoul/Then)
* [Alamofire](https://github.com/Alamofire/Alamofire)
* [ObjectMapper](https://github.com/Hearst-DD/ObjectMapper)
* [Swinject](https://github.com/Swinject/Swinject)
* [IQKeyboardManagerSwift](https://github.com/hackiftekhar/IQKeyboardManager)
* [Quick/Nimble](https://github.com/Quick/Quick)