//
//  Helpers.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/11/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

infix operator ~ { associativity left precedence 170 }

func ~<T> (@autoclosure lhs: () -> T, @autoclosure rhs: () -> T) -> T {
    if UIDevice.currentDevice().userInterfaceIdiom != .Pad {
        return lhs()
    }
    return rhs()
}