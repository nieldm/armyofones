//
//  String.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/11/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

extension String {
    var isNumeric: Bool {
        let predicate = NSPredicate(format:"SELF MATCHES '[0-9]+'")
        return predicate.evaluateWithObject(trimLeadingAndTrailingWhiteSpacesAndNewLines())
    }
    
    func trimLeadingAndTrailingWhiteSpacesAndNewLines() -> String {
        return stringByTrimmingCharactersInSet(.whitespaceAndNewlineCharacterSet())
    }
}