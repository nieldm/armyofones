//
//  UIColor.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/11/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func primary() -> UIColor {
        return UIColor(red: 239 / 255.0, green: 3 / 255.0, blue: 137 / 255.0, alpha: 1.0)
    }
    
    static func secondary() -> UIColor {
        return UIColor(red: 128 / 255.0, green: 128 / 255.0, blue: 128 / 255.0, alpha: 1.0)
    }
}