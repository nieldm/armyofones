//
//  UIFont.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/11/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

enum CopernicusFontWeight: String {
    case Book
    case BookItalic
}

extension UIFont {
    class func primary(size: CGFloat, weight: CopernicusFontWeight = .Book) -> UIFont {
        return UIFont(name: "Copernicus-\(weight)", size: size)!
    }
    
    class func secondary(size: CGFloat) -> UIFont {
        return UIFont(name: "HUGE-AvantGarde-Bold", size: size)!
    }
}