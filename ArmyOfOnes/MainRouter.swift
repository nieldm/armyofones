//
//  MainRouter.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/13/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

class MainRouter {
    var mainVC: MainViewController
    
    init(mainVC: MainViewController) {
        self.mainVC = mainVC
    }
    
    func dismissVC() {
        mainVC.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: ExchangeVC its here to be testable it will be better on a extension at the module container
    func showExchangeVC(amount: Int) {
        guard let vc = ExchangeContainer.container(amount).resolve(ExchangesViewController.self)
            else { return }
        mainVC.navigationController?.pushViewController(vc, animated: true)
    }
        
}