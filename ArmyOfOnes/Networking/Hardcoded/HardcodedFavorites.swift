//
//  HardcodedFavorites.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/13/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation

// its hardcoded but using and api interface to be easly replaceable
class HardCodedFavorites: APIFavorite {
    var data: [FavoriteCurrency] = [
        FavoriteCurrency(title: "GBP", currency: "GBP"),
        FavoriteCurrency(title: "EUR", currency: "EUR"),
        FavoriteCurrency(title: "JPY", currency: "JPY"),
        FavoriteCurrency(title: "BRL", currency: "BRL")
    ]
    
    func get(callback: ([FavoriteCurrency]) -> ()) {
        callback(data)
    }

}