//
//  AlamoContainer.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation
import Swinject
import Then

class AlamoContainer {
    
    class func container() -> Container {
        return Container() { c in
            c.register(Network.self) { r in
                let currency = r.resolve(AlamoCurrency.self)
                
                let network = Network().then {
                    $0.currency = currency
                }
                
                return network
            }
            
            c.register(AlamoCurrency.self) { r in AlamoCurrency() }
        }
    }
    
}