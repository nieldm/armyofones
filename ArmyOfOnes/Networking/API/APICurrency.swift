//
//  APICurrency.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation

enum BaseRates: String {
    case USD
}

//MARK: API
protocol APICurrency {
    func getExchangeRates(base: BaseRates, callback: (rates: ExchangeRates?) -> ())
}

//MARK: Models
protocol ExchangeRatesCompatible {
    func toExchangeRates() -> ExchangeRates
}

struct ExchangeRates {
    var base: String
    var date: String
    var rates: [Rate]
}

struct Rate {
    var currency: String
    var exchange: Float
}
