//
//  ExchangePresenter.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation

protocol ExchangePresenterInput {
    func loadData()
    func giveAmount(amount: Int)
    func didChangeCenterCell(data: Rate)
    func didCollectionViewBeginScrolling()
    func didClickFav(fav: FavoriteCurrency)
    func dismiss()
}

protocol ExchangePresenterOutput: class {
    func didCollectionViewBeginScrolling()
    func mainDataDidChange(data: Rate)
    func amountDidChange(amount: Int)
    func dataDidChange(data: ExchangeRates?)
    func favoritesDidChange(data: [FavoriteCurrency])
    func scrollToPosition(position: Int)
}

class ExchangePresenter: ExchangePresenterInput {
    
    var outputs: [ExchangePresenterOutput] = []
    var data: ExchangeRates?
    var favs: [FavoriteCurrency]?
    var interactor: ExchangeInteractorInput?
    
    func giveAmount(amount: Int) {
        outputs.forEach { $0.amountDidChange(amount) }
    }
    
    func didChangeCenterCell(data: Rate) {
        outputs.forEach { $0.mainDataDidChange(data) }
    }
    
    func didCollectionViewBeginScrolling() {
        outputs.forEach { $0.didCollectionViewBeginScrolling() }
    }
    
    func loadData() {
        guard let data = data,
            favs = favs
            else {
            interactor?.loadData()
            return
        }
        dataLoaded(data)
        favoritesLoaded(favs)
    }
    
    func didClickFav(fav: FavoriteCurrency) {
        var tmpIndex: Int?
        var tmpRate: Rate?
        data?.rates.enumerate().forEach { key, value in
            if value.currency == fav.currency {
                tmpIndex = key
                tmpRate = value
                return
            }
        }
        
        guard let rate = tmpRate,
                index = tmpIndex
                else { return }
        outputs.forEach { $0.scrollToPosition(index) }
        outputs.forEach { $0.mainDataDidChange(rate) }
    }
    
    func dismiss() {
        outputs = []
        interactor?.dismiss()
    }
    
}

extension ExchangePresenter: ExchangeInteractorOutput {
    func dataLoaded(data: ExchangeRates?) {
        outputs.forEach { $0.dataDidChange(data) }
        self.data = data
    }
    
    func favoritesLoaded(favs: [FavoriteCurrency]) {
        outputs.forEach { $0.favoritesDidChange(favs) }
        self.favs = favs
    }
}