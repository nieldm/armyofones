//
//  ExchangeViewCell.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

class ExchangeViewCell: UICollectionViewCell {

    private weak var currencyNameLabel: UILabel!
    private weak var currencyRateLabel: UILabel!
    private weak var currencyTotalLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: View Hierarchy
    private func buildView() {
        let mainView = UIView()
        
        contentView.addSubview(mainView)
        
        mainView.then {
            $0.backgroundColor = .clearColor()
            $0.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25).CGColor
            $0.layer.borderWidth = 10
            $0.layer.cornerRadius = 8
            $0.snp_makeConstraints { make in
                make.size.equalTo(self.contentView)
                make.center.equalTo(self.contentView)
            }
        }
        
        let currencyNameLabel = UILabel()
        
        mainView.addSubview(currencyNameLabel)
        
        currencyNameLabel.then {
            $0.font = .secondary(42 ~ 72)
            $0.text = "BITCOIN"
            $0.textAlignment = .Center
            $0.snp_makeConstraints { make in
                make.top.equalTo(mainView.snp_top).offset(45)
                make.centerX.equalTo(mainView)
                make.width.equalTo(mainView).multipliedBy(0.8)
            }
        }
        
        self.currencyNameLabel = currencyNameLabel
        
        let currencyNameSubTitleLabel = UILabel()
        
        mainView.addSubview(currencyNameSubTitleLabel)
        
        currencyNameSubTitleLabel.then {
            $0.textColor = .blackColor()
            $0.font = .primary(12 ~ 24)
            $0.text = "currency"
            $0.textAlignment = .Center
            $0.snp_makeConstraints { make in
                make.top.equalTo(currencyNameLabel.snp_bottom).offset(5)
                make.centerX.equalTo(mainView)
                make.width.equalTo(mainView).multipliedBy(0.8)
            }
        }
        
        let containerView = UIView()
        
        mainView.addSubview(containerView)
        
        containerView.then {
            $0.snp_makeConstraints { make in
                make.height.equalTo(mainView).multipliedBy(0.6)
                make.width.equalTo(mainView)
                make.centerX.equalTo(mainView)
                make.bottom.equalTo(mainView.snp_bottom)
            }
        }
        
        let currencyTotalLabel = UILabel()
        
        containerView.addSubview(currencyTotalLabel)
        
        currencyTotalLabel.then {
            $0.font = .secondary(62 ~ 82)
            $0.text = "$$$$$"
            $0.textAlignment = .Center
            $0.adjustsFontSizeToFitWidth = true
            $0.snp_makeConstraints { make in
                make.top.equalTo(containerView.snp_centerY)
                make.centerX.equalTo(containerView)
                make.width.equalTo(containerView).multipliedBy(0.8)
            }
        }
        
        self.currencyTotalLabel = currencyTotalLabel
        
        let currencyRateLabel = UILabel()
        
        containerView.addSubview(currencyRateLabel)
        
        currencyRateLabel.then {
            $0.textAlignment = .Center
            $0.font = .primary(12 ~ 24)
            $0.text = "total"
            $0.snp_makeConstraints { make in
                make.top.equalTo(currencyTotalLabel.snp_bottom)
                make.centerX.equalTo(containerView)
                make.width.equalTo(containerView).multipliedBy(0.8)
            }
        }
        
        self.currencyRateLabel = currencyRateLabel
    }
    
    func setData(data: Rate?, amount: Int?) {
        guard let data = data,
                amount = amount
                else { return }
        currencyNameLabel?.text = data.currency
        currencyRateLabel?.text = "rate: \(data.exchange)"
        let total = data.exchange * Float(amount)
        currencyTotalLabel?.text = "\(total)"
    }

}
