//
//  ExchangesViewController.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

private enum ZPositionOrder: CGFloat {
    case Bottom = 1, Middle, Top
}

private enum CitiesBGs: String {
    case EUR, GBP, BRL
}

final class ExchangesViewController: BaseViewController {
    
    
    private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.then {
                $0.backgroundColor = .clearColor()
                $0.layer.cornerRadius = 8
                $0.layer.zPosition = ZPositionOrder.Middle.rawValue
                dataSource.collectionView = $0
            }
        }
    }
    private weak var amountLabel: UILabel! {
        didSet {
            amountLabel.then {
                $0.text = "\(amount)"
                $0.font = .secondary(82 ~ 102)
                $0.textColor = .whiteColor()
                $0.textAlignment = .Center
                $0.adjustsFontSizeToFitWidth = true
            }
        }
    }
    private weak var amountSubTitleLabel: UILabel! {
        didSet {
            amountSubTitleLabel.then {
                $0.textAlignment = .Center
                $0.textColor = .whiteColor()
                $0.font = .primary(12 ~ 24)
                $0.text = "bucks"
            }
        }
    }
    
    private weak var imageContainer: UIView! {
        didSet {
            imageContainer.then {
                let layer = CAShapeLayer()
                $0.backgroundColor = .clearColor()
                $0.layer.zPosition = ZPositionOrder.Top.rawValue
                $0.layer.addSublayer(layer)
                imageLayer = layer
            }
        }
    }
    
    private var favoritesButtons: [UIButton]! {
        didSet {
            favoritesButtons.forEach {
                $0.then {
                    $0.setTitle("", forState: .Normal)
                    $0.titleLabel?.font = .primary(24 ~ 36)
                    $0.setTitleColor(.primary(), forState: .Normal)
                    $0.setTitleColor(.secondary(), forState: .Highlighted)
                    $0.backgroundColor = .whiteColor()
                    $0.layer.cornerRadius = 4
                    $0.hidden = true
                    $0.addTarget(self, action: #selector(favoritePressed(_:)), forControlEvents: .TouchUpInside)
                }
            }
        }
    }
    
    private weak var changeButton: UIButton! {
        didSet {
            changeButton.then {
                $0.setTitle("Edit my bucks", forState: .Normal)
                $0.titleLabel?.font = .primary(22 ~ 42)
                $0.setTitleColor(.blackColor(), forState: .Normal)
                $0.backgroundColor = .whiteColor()
                $0.layer.zPosition = ZPositionOrder.Top.rawValue
                $0.addTarget(self, action: #selector(changePressed), forControlEvents: .TouchUpInside)
                $0.layer.cornerRadius = 5
            }
        }
    }
    
    private weak var retryButton: UIButton! {
        didSet {
            retryButton.then {
                $0.setTitle("Reload", forState: .Normal)
                $0.titleLabel?.font = .primary(12 ~ 22)
                $0.setTitleColor(.blackColor(), forState: .Normal)
                $0.backgroundColor = .clearColor()
                $0.layer.zPosition = ZPositionOrder.Top.rawValue
                $0.addTarget(self, action: #selector(retryPressed), forControlEvents: .TouchUpInside)
            }
        }
    }
    
    private var imageLayer: CAShapeLayer?
    var presenter: ExchangePresenterInput?
    var dataSource: ExchangesDataSource
    var favs: [FavoriteCurrency] = []
    private var amount: Int
    
    init(amount: Int, dataSource: ExchangesDataSource?) {
        self.amount = amount
        self.dataSource = dataSource ?? ExchangesDataSource(amount: amount)
        super.init(nibName: nil, bundle: nil)
        buildView()
        style()
    }
    
    required init?(coder aDecoder: NSCoder) {
        amount = 0
        dataSource = ExchangesDataSource(amount: amount)
        super.init(coder: aDecoder)
    }
    
    //MARK: View Hierarchy
    private func buildView() {
        
        let layout = UICollectionViewLayout()
        
        let _ = UIView().then {
            view.addSubview($0)
            $0.snp_makeConstraints { make in
                make.width.equalTo(view)
                make.bottom.equalTo(view)
                make.height.equalTo(view).multipliedBy(0.55 ~ 0.5)
                make.centerX.equalTo(view)
            }
            self.imageContainer = $0
        }
        
        let collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: layout)
            .then {
            view.addSubview($0)
            $0.snp_makeConstraints { make in
                make.height.equalTo(view).multipliedBy(0.8)
                make.width.equalTo(view).multipliedBy(0.85)
                make.center.equalTo(view)
            }
            self.collectionView = $0
        }
        
        let amountLabel = UILabel()
            .then {
            view.addSubview($0)
            $0.snp_makeConstraints { make in
                make.center.equalTo(collectionView)
                make.width.equalTo(collectionView)
            }
            self.amountLabel = $0
        }
        
        let _ = UIButton()
            .then {
            view.addSubview($0)
            $0.snp_makeConstraints { make in
                make.height.equalTo(45 ~ 68)
                make.bottom.equalTo(view.snp_bottom).offset(5)
                make.width.equalTo(view).multipliedBy(0.85)
                make.centerX.equalTo(view)
            }
            self.changeButton = $0
        }
        
        let _ = UILabel()
            .then {
            view.addSubview($0)
            $0.snp_makeConstraints { make in
                make.top.equalTo(amountLabel.snp_bottom).inset(5)
                make.width.equalTo(amountLabel)
                make.centerX.equalTo(amountLabel)
            }
            self.amountSubTitleLabel = $0
        }
        
        var lastButton: UIButton?
        let numOfButtons = 4
        self.favoritesButtons = []
        for pos in 1...numOfButtons {
            let _ = UIButton().then {
                view.addSubview($0)
                $0.snp_makeConstraints { make in
                    make.height.equalTo(35 ~ 65)
                    if let lastButton = lastButton {
                        make.left.equalTo(lastButton.snp_right).offset(5)
                        make.width.equalTo(lastButton)
                    } else {
                        make.left.equalTo(view).offset(10)
                    }
                    make.top.equalTo(view).offset(5)
                    if pos == numOfButtons {
                        make.right.equalTo(view).inset(10)
                    }
                }
                self.favoritesButtons.append($0)
                lastButton = $0
            }
        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.loadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    private func style() {
        view.then {
            $0.backgroundColor = .primary()
        }
    }
    
    private func addBackgroundAnimation(path: (CGRect) -> UIBezierPath) {
        guard let layer = imageLayer else { return }
        layer.path = path(imageContainer.bounds).CGPath
        layer.strokeColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25).CGColor
        layer.lineWidth = 3
        layer.fillColor = UIColor.whiteColor().CGColor
        
        let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 5.0
        pathAnimation.fromValue = 0.0
        pathAnimation.toValue = 1.0
        
        let fillAnimation = CABasicAnimation(keyPath: "fillColor")
        fillAnimation.duration = 8.0
        fillAnimation.fromValue = UIColor.clearColor().CGColor
        fillAnimation.toValue = UIColor.whiteColor().CGColor
        
        layer.addAnimation(pathAnimation, forKey: nil)
        layer.addAnimation(fillAnimation, forKey: nil)
    }

    private func changeImageContainerVisibility(show: Bool = true) {
        let hideAnimation = CABasicAnimation(keyPath: "opacity").then {
            $0.fromValue = imageLayer?.opacity
            $0.toValue = show ? 1.0 : 0.0
        }
        
        imageLayer?.addAnimation(hideAnimation, forKey: nil)
        imageLayer?.opacity = show ? 1.0 : 0.0
    }
    
    func favoritePressed(button: UIButton) {
        changeImageContainerVisibility(false)
        let tag = button.tag
        if favs.count - 1 < tag { return }
        let fav = favs[tag]
        presenter?.didClickFav(fav)
    }
    
    func changePressed() {
        presenter?.dismiss()
    }
    
    func retryPressed() {
        presenter?.loadData()
    }
}

extension ExchangesViewController: ExchangePresenterOutput {
    func didCollectionViewBeginScrolling() {
        changeImageContainerVisibility(false)
    }
    func mainDataDidChange(data: Rate) {
        let exists = favs.filter { $0.currency == data.currency }
        if exists.count > 0 {
            guard let bg = CitiesBGs(rawValue: data.currency) else { return }
            defer { changeImageContainerVisibility() }
            switch bg {
            case .BRL:
                addBackgroundAnimation(Path.getRio)
            case .GBP:
                addBackgroundAnimation(Path.getLondon)
            default:
                addBackgroundAnimation(Path.getBerlin)
            }
            return
        }
    }
    func favoritesDidChange(data: [FavoriteCurrency]) {
        favs = data
        data.enumerate().forEach { key, fav in
            guard let favoritesButtons = favoritesButtons else { return }
            if favoritesButtons.count - 1 < key { return }
            favoritesButtons[key].then {
                $0.setTitle(fav.title, forState: .Normal)
                $0.tag = key
                $0.hidden = false
            }
        }
    }
    func amountDidChange(amount: Int) {}
    func dataDidChange(data: ExchangeRates?) {
        guard let _ = data else {
            retryButton?.hidden = false
            return
        }
        retryButton?.hidden = true
    }
    func scrollToPosition(position: Int) {}
}
