//
//  ExchangeContainer.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/13/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Swinject

class ExchangeContainer {
    
    class func container(amount: Int) -> Container {
        return Container() { c in
            c.register(ExchangesViewController.self) { r in
                let presenter = r.resolve(ExchangePresenter.self)
                let dataSource = r.resolve(ExchangesDataSource.self)
                let vc = ExchangesViewController(amount: amount, dataSource: dataSource)
                vc.presenter = presenter
                presenter?.outputs.append(vc)
                return vc
            }
            
            c.register(ExchangesDataSource.self) { r in
                let dataSource = ExchangesDataSource(amount: amount)
                let presenter = r.resolve(ExchangePresenter.self)
                dataSource.presenter = presenter
                presenter?.outputs.append(dataSource)
                return dataSource
            }.inObjectScope(.Container)
            
            c.register(ExchangePresenter.self) { r in
                let interactor = r.resolve(ExchangeInteractor.self)
                let presenter = ExchangePresenter()
                presenter.interactor = interactor
                interactor?.output = presenter
                return presenter
            }.inObjectScope(.Container)
            
            c.register(ExchangeInteractor.self) { r in
                let router = r.resolve(MainRouter.self)
                guard let network = r.resolve(Network.self) else {
                    let interactor = ExchangeInteractor(network: Network())
                    interactor.router = router
                    return interactor
                }
                
                let interactor = ExchangeInteractor(network: network)
                interactor.router = router
                return interactor
            }
            
            c.register(MainRouter.self) { r in
                let appDelegate = r.resolve(AppDelegate.self)
                guard let router = appDelegate?.container.resolve(MainRouter.self)
                    else { return MainRouter(mainVC: MainViewController()) }
                return router
            }
            
            c.register(APICurrency.self) { r in
                return AlamoCurrency()
            }
            
            c.register(APIFavorite.self) { r in
                return HardCodedFavorites()
            }
            
            c.register(Network.self) { r in
                let network = Network().then {
                    $0.currency = r.resolve(APICurrency.self)
                    $0.favorite = r.resolve(APIFavorite.self)
                }
                return network
            }
            
            c.register(AppDelegate.self) { r in
                return (UIApplication.sharedApplication().delegate as! AppDelegate)
            }
        }
    }
    
}