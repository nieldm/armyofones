//
//  ExchangesDataSource.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

private enum ExchangesDataSourceCells: String {
    case ExchangeViewCell
}

class ExchangesDataSource: NSObject, UICollectionViewDataSource {
    var presenter: ExchangePresenterInput? {
        didSet {
            presenter?.loadData()
        }
    }
    var data: ExchangeRates? {
        didSet {
            collectionView?.reloadData()
        }
    }
    var collectionView: UICollectionView! {
        didSet {
            collectionView.then {
                let flowLayout = UICollectionViewFlowLayout().then {
                    $0.scrollDirection = .Horizontal
                    $0.footerReferenceSize = CGSize.zero
                    $0.headerReferenceSize = CGSize.zero
                    $0.sectionInset = UIEdgeInsets()
                    $0.minimumInteritemSpacing = 0.0
                    $0.minimumLineSpacing = 0.0
                }
                $0.setCollectionViewLayout(flowLayout, animated: false)
                $0.delegate = self
                $0.dataSource = self
                $0.pagingEnabled = true
                $0.registerClass(ExchangeViewCell.self, forCellWithReuseIdentifier: ExchangesDataSourceCells.ExchangeViewCell.rawValue)
                $0.reloadData()
            }
        }
    }
    var amount: Int
    
    init(amount: Int) {
        self.amount = amount
        super.init()
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data?.rates.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let data = self.data?.rates[indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ExchangesDataSourceCells.ExchangeViewCell.rawValue, forIndexPath: indexPath)
        (cell as? ExchangeViewCell)?.setData(data, amount: amount)
        return cell
    }
}

extension ExchangesDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(
            width: collectionView.bounds.width,
            height: collectionView.bounds.height)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        guard let cell = collectionView.visibleCells().first as? ExchangeViewCell,
            indexPath = collectionView.indexPathForCell(cell),
            data = self.data?.rates[indexPath.row]
            else { return }
        presenter?.didChangeCenterCell(data)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        presenter?.didCollectionViewBeginScrolling()
    }
    
}

extension ExchangesDataSource: ExchangePresenterOutput {
    func didCollectionViewBeginScrolling() {}
    func mainDataDidChange(data: Rate) {}
    func amountDidChange(amount: Int) {}
    func dataDidChange(data: ExchangeRates?) {
        self.data = data
    }
    func favoritesDidChange(data: [FavoriteCurrency]) {}
    func scrollToPosition(position: Int) {
        let indexPath = NSIndexPath(forRow: position, inSection: 0)
        collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
    }
}