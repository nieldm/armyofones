//
//  ExchangeInteractor.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation

protocol ExchangeInteractorInput {
    func loadData()
    func dismiss()
}

protocol ExchangeInteractorOutput: class {
    func dataLoaded(data: ExchangeRates?)
    func favoritesLoaded(favs: [FavoriteCurrency])
}

class ExchangeInteractor {
    var router: MainRouter?
    var network: Network
    weak var output: ExchangeInteractorOutput?
    
    init(network: Network) {
        self.network = network
    }
    
}

extension ExchangeInteractor: ExchangeInteractorInput {
    func loadData() {
        network.currency?.getExchangeRates(.USD) { data in
            self.output?.dataLoaded(data)
        }
        network.favorite?.get { favs in
            self.output?.favoritesLoaded(favs)
        }
    }
    
    func dismiss() {
        router?.dismissVC()
    }
}