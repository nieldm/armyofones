//
//  ExchangeInteractorSpec.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/13/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Quick
import Nimble
@testable import ArmyOfOnes

class ExchangeInteractorSpec: QuickSpec {
    
    override func spec() {
        describe("the exchange interactor") {
            let container = ExchangeContainer.container(0)
            container.register(AppDelegate.self) { r in
                return AppDelegate()
            }
            container.register(MainRouter.self) { r in
                return MockRouter()
            }
            container.register(APICurrency.self) { r in
                return MockApiCurrency()
            }
            container.register(APIFavorite.self) { r in
                return MockApiFavorite()
            }

            let output: MockOutput? = MockOutput()
            let interactor = container.resolve(ExchangeInteractor.self)
            interactor?.output = output
            describe("calls") {
                it("loadData") {
                    interactor?.loadData()
                    expect(output?.data?.base).to(equal("TEST"))
                    expect(output?.favs?.first?.title).to(equal("TEST"))
                }
                it("dismiss") {
                    interactor?.dismiss()
                    let mockRouter = interactor?.router as? MockRouter
                    expect(mockRouter?.vcDismissed).to(equal(true))
                }
            }
        }
    }
    
}

private class MockRouter: MainRouter {
    var vcDismissed = false
    
    override func dismissVC() {
        vcDismissed = true
    }
    
    init() {
        super.init(mainVC: MainViewController())
    }
}

private class MockApiCurrency: APICurrency {
    func getExchangeRates(base: BaseRates, callback: (rates: ExchangeRates?) -> ()) {
        callback(rates: ExchangeRates(base: "TEST", date: "TEST", rates: []))
    }
}

private class MockApiFavorite: APIFavorite {
    func get(callback: ([FavoriteCurrency]) -> ()) {
        callback([FavoriteCurrency(title: "TEST", currency: "TEST")])
    }
}

private class MockOutput: ExchangeInteractorOutput {
    var data: ExchangeRates?
    var favs: [FavoriteCurrency]?
    
    func dataLoaded(data: ExchangeRates?) {
        self.data = data
    }
    
    func favoritesLoaded(favs: [FavoriteCurrency]) {
        self.favs = favs
    }
}