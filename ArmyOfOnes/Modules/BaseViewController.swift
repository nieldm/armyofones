//
//  BaseViewController.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/11/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    //MARK: Override
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

}
