//
//  RequestInputPresenter.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/13/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation

protocol RequestInputPresenterInput {
    func changeModel(newModel: Int)
    func modifyModel(amount: Int)
    func didFinish()
    func loadData()
}

protocol RequestInputPresenterOutput {
    func modelDidUpdate(model: Int)
}

class RequestInputPresenter: RequestInputPresenterInput {
    
    var output: RequestInputPresenterOutput?
    var interactor: RequestInputInteractorInput?
    
    func changeModel(newModel: Int) {
        interactor?.changeModel(newModel)
    }
    
    func modifyModel(amount: Int) {
        interactor?.modifyModel(amount)
    }
    
    func didFinish() {
        interactor?.goToExchange()
    }
    
    func loadData() {
        interactor?.loadData()
    }
    
    deinit {
        output = nil
        interactor = nil
    }
}

extension RequestInputPresenter: RequestInputInteractorOutput {
    func modelDidChange(model: Int) {
        output?.modelDidUpdate(model)
    }
    
    
}