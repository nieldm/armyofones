//
//  RequestInputViewController.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/11/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit

private enum BuckButton: Int {
    case Plus = 0, Minus
}

//MARK: Config Values
private let topAmountForSize: CGFloat = 1000
private let minFontSize: CGFloat = 24 ~ 62
private let maxFontSize: CGFloat = 92 ~ 122
private let firstStepDelay: Double = 3
private let secondStepDelay: Double = 6

final class RequestInputViewController: BaseViewController {

    //MARK: Outlets
    private weak var bucksTextInput: UITextField!
    private weak var titleLabel: UILabel!
    private weak var minusButton: UIButton!
    private weak var plusButton: UIButton!
    private weak var continueButton: UIButton!

    //MARK: Vars
    var presenter: RequestInputPresenterInput?

    private var longPressTimer: NSTimer?
    private var longPressTimerStepOne: NSTimer?
    private var longPressTimerStepTwo: NSTimer?
    private var timerInterval = 0.5
    private var action = Selector?()

    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        buildView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.loadData()
        titleLabel?.becomeFirstResponder()
    }
    
    //MARK: View Hierarchy
    private func buildView() {
        let mainView = UIView()
        
        view.addSubview(mainView)
        
        mainView.then {
            $0.backgroundColor = .whiteColor()
            $0.snp_makeConstraints { make in
                make.size.equalTo(self.view)
                make.center.equalTo(self.view)
            }
        }
        
        let titleLabel = UILabel()
        
        mainView.addSubview(titleLabel)
        
        titleLabel.then {
            $0.numberOfLines = 0
            $0.font = .secondary(42 ~ 62)
            $0.text = "Who many bucks do you have?"
            $0.textAlignment = .Left ~ .Center
            $0.snp_makeConstraints { make in
                make.centerX.equalTo(mainView)
                make.top.equalTo(mainView).offset(35 ~ 75)
                make.width.equalTo(mainView).multipliedBy(0.9)
            }
        }
        
        self.titleLabel = titleLabel
        
        let bucksTextInput = UITextField()
        
        mainView.addSubview(bucksTextInput)
        
        bucksTextInput.then {
            $0.font = .primary(minFontSize)
            $0.addTarget(self, action: #selector(textFieldDidChangeText(_:)), forControlEvents: UIControlEvents.EditingChanged)
            $0.delegate = self
            $0.tintColor = .primary()
            $0.keyboardType = .NumberPad
            $0.textAlignment = .Center
            $0.adjustsFontSizeToFitWidth = true
            $0.minimumFontSize = 10
            $0.snp_makeConstraints { make in
                make.center.equalTo(mainView)
                make.width.equalTo(mainView).multipliedBy(0.9)
            }
        }
        
        self.bucksTextInput = bucksTextInput
        
        let continueButton = UIButton()
        
        mainView.addSubview(continueButton)
        
        continueButton.then {
            $0.setTitle("Show me those rates", forState: .Normal)
            $0.titleLabel?.font = .primary(22 ~ 42)
            $0.setTitleColor(.primary(), forState: .Normal)
            $0.setTitleColor(.secondary(), forState: .Highlighted)
            $0.backgroundColor = .clearColor()
            $0.layer.cornerRadius = 4
            $0.addTarget(self, action: #selector(continuePressed), forControlEvents: .TouchUpInside)
            $0.snp_makeConstraints { make in
                make.centerX.equalTo(mainView)
                make.width.equalTo(mainView).multipliedBy(0.8)
                make.height.equalTo(48 ~ 64)
                make.bottom.equalTo(mainView).inset(15)
            }
        }
        
        self.continueButton = continueButton
        
        let minusButton = UIButton()
        
        mainView.addSubview(minusButton)
        
        let plusButton = UIButton()
        
        mainView.addSubview(plusButton)
        
        minusButton.then {
            $0.setImage(
                UIImage(named: "minus")?.imageWithRenderingMode(.AlwaysTemplate),
                forState: .Normal)
            $0.imageView?.tintColor = .whiteColor()
            $0.titleLabel?.font = .primary(42 ~ 62)
            $0.setTitleColor(.whiteColor(), forState: .Normal)
            $0.setTitleColor(.secondary(), forState: .Highlighted)
            $0.backgroundColor = .primary()
            $0.tag = BuckButton.Minus.rawValue
            $0.layer.cornerRadius = 4
            $0.addTarget(self, action: #selector(buttonPressed(_:)), forControlEvents: .TouchUpInside)
            $0.addTarget(self, action: #selector(longPressButton(_:)), forControlEvents: .TouchDown)
            $0.snp_makeConstraints { make in
                make.leading.equalTo(bucksTextInput.snp_leading).offset(15)
                make.top.equalTo(bucksTextInput.snp_bottom).offset(15)
                make.height.equalTo(48 ~ 84)
                make.width.equalTo(plusButton)
                make.trailing.equalTo(mainView.snp_centerXWithinMargins).offset(-4)
            }
        }
        
        plusButton.then {
            $0.setImage(
                UIImage(named: "plus")?.imageWithRenderingMode(.AlwaysTemplate),
                forState: .Normal)
            $0.tintColor = .whiteColor()
            $0.titleLabel?.font = .primary(42 ~ 62)
            $0.setTitleColor(.whiteColor(), forState: .Normal)
            $0.setTitleColor(.secondary(), forState: .Highlighted)
            $0.backgroundColor = .primary()
            $0.layer.cornerRadius = 4
            $0.tag = BuckButton.Plus.rawValue
            $0.addTarget(self, action: #selector(buttonPressed(_:)), forControlEvents: .TouchUpInside)
            $0.addTarget(self, action: #selector(longPressButton(_:)), forControlEvents: .TouchDown)
            $0.snp_makeConstraints { make in
                make.trailing.equalTo(bucksTextInput.snp_trailing).inset(15)
                make.top.equalTo(bucksTextInput.snp_bottom).offset(15)
                make.height.equalTo(48 ~ 84)
                make.width.equalTo(minusButton)
                make.leading.equalTo(mainView.snp_centerXWithinMargins).offset(4)
            }
        }
        
        self.minusButton = minusButton
        self.plusButton = plusButton
    }
    
    //MARK: Behavior
    func textFieldDidChangeText(textField: UITextField) {
        guard let text = textField.text,
            value = Int(text)
            else { return }
        presenter?.changeModel(value)
    }
    
    private func updateFontSize(model: Int) {
        let progress: CGFloat = CGFloat(model) / topAmountForSize,
            maxFont: CGFloat  = maxFontSize,
            minFont: CGFloat  = minFontSize,
            size = min(max(minFont, maxFont * progress), maxFont)
        bucksTextInput.font = UIFont.primary(size)
    }
    
    func buttonPressed(button: UIButton) {
        defer { bounce(button) }
        longPressTimer?.invalidate()
        longPressTimer = nil
        longPressTimerStepOne?.invalidate()
        longPressTimerStepOne = nil
        longPressTimerStepTwo?.invalidate()
        longPressTimerStepTwo = nil
        guard let buckButton = BuckButton(rawValue: button.tag) else { return }
        switch buckButton {
        case .Plus:
            plus()
            return
        case .Minus:
            minus()
            return
        }
    }
    
    func getAmount() -> Int {
        guard let text = bucksTextInput.text,
            amount = Int(text)
            else { return 0 }
        return amount
    }
    
    func continuePressed() {
        presenter?.didFinish()
    }
    
    func minus() {
        presenter?.modifyModel(-1)
    }
    
    
    func plus() {
        presenter?.modifyModel(+1)
    }
    
    private func bounce(view: UIView?) {
        guard let view = view else { return }
        let origin:CGPoint = view.center
        let target:CGPoint = CGPointMake(view.center.x, view.center.y + 4)
        let bounce = CABasicAnimation(keyPath: "position.y")
        bounce.duration = 0.1
        bounce.fromValue = origin.y
        bounce.toValue = target.y
        bounce.repeatCount = 1
        bounce.autoreverses = true
        bounce.removedOnCompletion = true
        view.layer.addAnimation(bounce, forKey: nil)
    }
    
    func longPressButton(button: UIButton) {
        bucksTextInput.resignFirstResponder()
        guard let buckButton = BuckButton(rawValue: button.tag) else { return }
        switch buckButton {
        case .Plus:
            createTimer(#selector(plus))
            return
        case .Minus:
            createTimer(#selector(minus))
            return
        }
    }
    
    private func createTimer(action: Selector) {
        self.action = action
        changeTimer(timerInterval, action: action)
        longPressTimerStepOne = NSTimer.scheduledTimerWithTimeInterval(firstStepDelay, target: self, selector: #selector(increaseTimerSpeedStepOne), userInfo: nil, repeats: false)
        longPressTimerStepTwo = NSTimer.scheduledTimerWithTimeInterval(secondStepDelay, target: self, selector: #selector(increaseTimerSpeedStepTwo), userInfo: nil, repeats: false)
    }
    
    private func changeTimer(interval: Double, action: Selector) {
        longPressTimer?.invalidate()
        longPressTimer = nil
        longPressTimer = NSTimer.scheduledTimerWithTimeInterval(interval, target: self, selector: action, userInfo: nil, repeats: true)
    }
    
    func increaseTimerSpeedStepOne() {
        guard let action = action else { return }
        changeTimer(timerInterval / 4, action: action)
    }
    
    func increaseTimerSpeedStepTwo() {
        guard let action = action else { return }
        changeTimer(timerInterval / 8, action: action)
    }
}

extension RequestInputViewController: RequestInputPresenterOutput {
    func modelDidUpdate(model: Int) {
        bucksTextInput.text = "\(model)"
        updateFontSize(model)
        bounce(bucksTextInput)
    }
}

//MARK: UITextFieldDelegate
extension RequestInputViewController: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard !string.isEmpty else { return true }
        guard let finalText = (textField.text as NSString?)?.stringByReplacingCharactersInRange(range, withString: string)
            else { return false }
        return finalText.isNumeric
    }
}