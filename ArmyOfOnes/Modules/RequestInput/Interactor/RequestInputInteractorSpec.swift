//
//  RequestInputInteractorSpec.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/13/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import XCTest
import Quick
import Nimble
@testable import ArmyOfOnes

class RequestInputInteractorSpec: QuickSpec {
    
    override func spec() {
        describe("the request interactor") {
            var api = MockApiBuck()
            var router = MockRouter()
            
            let container = RequestInputContainer.container()
            container.register(AppDelegate.self) { r in
                return AppDelegate()
            }
            container.register(MainRouter.self) { r in
                return router
            }
            container.register(APIBuck.self) { r in
                return api
            }
            
            var output = MockOutput()
            var interactor = container.resolve(RequestInputInteractor.self)
            interactor?.output = output
            

            beforeEach {
                api = MockApiBuck()
                router = MockRouter()
                output = MockOutput()
                interactor = container.resolve(RequestInputInteractor.self)
                interactor?.output = output
            }
            describe("calls") {
                it("loadData") {
                    interactor?.loadData()
                    expect(api.getCalled).to(equal(true))
                    expect(api.setCalled).to(equal(false))
                    expect(output.model).to(equal(161188))
                }
                it("changeModel") {
                    interactor?.changeModel(18589)
                    expect(output.model).to(equal(18589))
                    interactor?.changeModel(-12312313)
                    expect(output.model).to(equal(0))
                }
                it("modifyModel") {
                    interactor?.modifyModel(-10)
                    expect(output.model).to(equal(32))
                }
                it("modifyModel with negative value") {
                    interactor?.modifyModel(-100)
                    expect(output.model).to(equal(0))
                }
                it("goToExchange") {
                    interactor?.goToExchange()
                    expect(api.buck?.total).to(equal(42))
                    expect(router.showExchangeVCCalled).to(equal(true))
                }
                it("goToExchange with negative value") {
                    interactor?.changeModel(0)
                    interactor?.goToExchange()
                    expect(api.buck?.total).to(equal(0))
                    expect(router.showExchangeVCCalled).to(equal(false))
                }
            }
        }
    }
    
}
private class MockRouter: MainRouter {
    var showExchangeVCCalled = false
    
    override func showExchangeVC(amount: Int) {
        showExchangeVCCalled = true
    }
    
    init() {
        super.init(mainVC: MainViewController())
    }
}

private class MockOutput: RequestInputInteractorOutput {
    var model: Int?
    
    private func modelDidChange(model: Int) {
        self.model = model
    }
}

class MockApiBuck: APIBuck {
    var getCalled = false
    var setCalled = false
    var buck: Buck?
    
    func get() -> Buck? {
        getCalled = true
        return Buck(total: 161188, date: NSDate())
    }
    
    func set(buck: Buck) {
        setCalled = true
        self.buck = buck
    }
}