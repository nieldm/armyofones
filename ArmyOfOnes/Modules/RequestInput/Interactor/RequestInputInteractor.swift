//
//  RequestInputInteractor.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/13/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation

protocol RequestInputInteractorInput {
    func changeModel(newModel: Int)
    func modifyModel(amount: Int)
    func loadData()
    func goToExchange()
}

protocol RequestInputInteractorOutput: class {
    func modelDidChange(model: Int)
}

class RequestInputInteractor {
    var network: Network?
    weak var router: MainRouter?
    weak var output: RequestInputInteractorOutput?
    private var bucks = 42
    
}

extension RequestInputInteractor: RequestInputInteractorInput {
    func changeModel(newModel: Int) {
        let value = max(0, newModel)
        bucks = value
        output?.modelDidChange(bucks)
    }
    
    func modifyModel(amount: Int) {
        let value = max(0, bucks+amount)
        bucks = value
        output?.modelDidChange(bucks)
    }
    
    func loadData() {
        defer { output?.modelDidChange(bucks) }
        guard let savedBuck = network?.buck?.get() else {
            return
        }
        bucks = savedBuck.total
    }
    
    func goToExchange() {
        network?.buck?.set(Buck(total: bucks, date: NSDate()))
        if bucks <= 0 {
            output?.modelDidChange(bucks)
            return
        }
        router?.showExchangeVC(bucks)
    }
}