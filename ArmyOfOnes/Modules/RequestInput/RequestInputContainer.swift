//
//  RequestInputContainer.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/11/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Swinject

class RequestInputContainer {

    class func container() -> Container {
        return Container() { c in
            c.register(RequestInputViewController.self) { r in
                let presenter = r.resolve(RequestInputPresenter.self)
                
                let vc = RequestInputViewController()
                
                vc.presenter = presenter
                presenter?.output = vc
                return vc
            }
            
            c.register(RequestInputPresenter.self) { r in
                let interactor = r.resolve(RequestInputInteractor.self)
                
                let presenter = RequestInputPresenter()

                interactor?.output = presenter
                presenter.interactor = interactor
                return presenter
            }
            
            c.register(RequestInputInteractor.self) { r in
                let router = r.resolve(MainRouter.self)
                let network = r.resolve(Network.self)
                
                let interactor = RequestInputInteractor()
                
                interactor.network = network
                interactor.router = router
                return interactor
            }
            
            c.register(AppDelegate.self) { r in
                return (UIApplication.sharedApplication().delegate as! AppDelegate)
            }
            
            c.register(MainRouter.self) { r in
                let appDelegate = r.resolve(AppDelegate.self)
                guard let router = appDelegate?.container.resolve(MainRouter.self)
                    else { return MainRouter(mainVC: MainViewController()) }
                return router
            }
            
            c.register(APIBuck.self) { r in
                return UserDefaultBuck()
            }
            
            c.register(Network.self) { r in
                let network = Network().then {
                    $0.buck = r.resolve(APIBuck.self)
                }
                return network
            }
        }
    }
    
}

//MARK: Add to router
extension MainRouter {
    
    func showRequestInput() {
        guard let vc = RequestInputContainer.container().resolve(RequestInputViewController.self)
            else { return }
        mainVC.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}