//
//  Network.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Then

class Network: Then {
    var currency: APICurrency?
    var buck: APIBuck?
    var favorite: APIFavorite?
}