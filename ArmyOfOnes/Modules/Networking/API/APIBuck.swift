//
//  APIBuck.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation

protocol APIBuck {
    func get() -> Buck?
    func set(buck: Buck)
}

//MARK: Models
protocol BuckCompatible {
    func toBuck() -> Buck
}

struct Buck {
    var total: Int
    var date: NSDate?
}