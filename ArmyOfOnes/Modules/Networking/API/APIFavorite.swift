//
//  APIFavorite.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/13/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation

protocol APIFavorite {
    func get(callback: ([FavoriteCurrency]) -> ())
}

struct FavoriteCurrency {
    var title: String
    var currency: String
}