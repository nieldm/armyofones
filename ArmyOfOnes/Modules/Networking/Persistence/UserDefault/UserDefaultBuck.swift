//
//  UserDefaultBuck.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import ObjectMapper

private let kBuck = "Buck"

class UserDefaultBuck: APIBuck {
    func set(buck: Buck) {
        let mBuck = MUserDefaultBuck().then {
            $0.bucks = buck.total
            $0.date = buck.date
        }
        guard let json = mBuck.toJSONString() else { return }
        NSUserDefaults.standardUserDefaults().setObject(json, forKey: kBuck)
    }
    
    func get() -> Buck? {
        guard let json = NSUserDefaults.standardUserDefaults().objectForKey(kBuck) as? String,
            mBuck = Mapper<MUserDefaultBuck>().map(json)
            else { return nil }
        return mBuck.toBuck()
    }
    
}