//
//  MUserDefaultBuck.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Foundation
import ObjectMapper
import Then

class MUserDefaultBuck: Mappable, BuckCompatible, Then {
    var bucks: Int = 0
    var date: NSDate?
    
    init() {}
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        bucks <- map["bucks"]
        date <- map["date"]
    }
    
    func toBuck() -> Buck {
        return Buck(total: bucks, date: date)
    }
}