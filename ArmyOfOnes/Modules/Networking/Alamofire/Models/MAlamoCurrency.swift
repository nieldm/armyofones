//
//  MAlamoCurrency.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import ObjectMapper

class AlamoExchangeRates: Mappable, ExchangeRatesCompatible {
    var base: String = ""
    var date: String = ""
    var rates: NSDictionary?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        base  <- map["base"]
        date  <- map["date"]
        rates <- map["rates"]
    }
    
    func toExchangeRates() -> ExchangeRates {
        var rates: [Rate] = []
        self.rates?.forEach { key, value in
            print(key, value)
            guard let key = key as? String,
                value = value as? Float
                else { return }
            rates.append(Rate(currency: key, exchange: value))
        }
        return ExchangeRates(base: base, date: date,
                             rates: rates)
    }
    
}