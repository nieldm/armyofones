//
//  AlamoCurrency.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/12/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import Alamofire
import ObjectMapper

private enum AlamoCurrencyUrls: String {
    case GET = "http://api.fixer.io/latest?base="
}

class AlamoCurrency: APICurrency {
    func getExchangeRates(base: BaseRates, callback: (rates: ExchangeRates?) -> ()) {
        let url = AlamoCurrencyUrls.GET.rawValue + base.rawValue
        Alamofire
            .request(.GET, url)
            .responseJSON { response in
                print(response.result.value)
                let data = Mapper<AlamoExchangeRates>().map(response.result.value)
                callback(rates: data?.toExchangeRates())
        }
    }
}