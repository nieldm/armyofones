//
//  MainViewController.swift
//  ArmyOfOnes
//
//  Created by Daniel Mendez on 8/11/16.
//  Copyright © 2016 Daniel Mendez. All rights reserved.
//

import UIKit
import Then
import SnapKit

final class MainViewController: BaseViewController {
    
    //MARK: Outlets
    private weak var logo: UIImageView!
    private weak var contentView: UIView!
    private weak var mainTitle: UILabel!
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        buildView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        animateLogo()
        animateBackground()
    }
    
    //MARK: View Hierarchy
    private func buildView() {
        let mainView = UIView()
        
        view.addSubview(mainView)
        
        mainView.then {
            $0.backgroundColor = .whiteColor()
            $0.snp_makeConstraints { make in
                make.size.equalTo(self.view)
                make.center.equalTo(self.view)
            }
        }
        
        let logo = UIImageView()
        
        mainView.addSubview(logo)
        
        logo.then {
            $0.image = UIImage(named: "logo")
            $0.contentMode = .ScaleAspectFit
            $0.snp_makeConstraints { make in
                make.center.equalTo(mainView)
                make.size.equalTo(mainView).multipliedBy(0.6)
            }
        }
        self.logo = logo
        
        let contentView = UIView()
        
        mainView.addSubview(contentView)
        
        contentView.then {
            $0.layer.opacity = 0
            $0.backgroundColor = UIColor.primary()
            $0.snp_makeConstraints { make in
                make.center.equalTo(mainView)
                make.size.equalTo(mainView)
            }
        }
        
        self.contentView = contentView
        
        let label = UILabel()
        
        contentView.addSubview(label)
        
        label.then {
            $0.text = "Army of ones"
            $0.layer.opacity = 0
            $0.textColor = UIColor.whiteColor()
            $0.backgroundColor = UIColor.clearColor()
            $0.font = UIFont.secondary(62 ~ 82)
            $0.numberOfLines = 0
            $0.textAlignment = .Center
            $0.snp_makeConstraints { make in
                make.center.equalTo(contentView)
                make.width.equalTo(contentView).multipliedBy(0.9)
            }
        }
        
        mainTitle = label
    }
    
    //MARK: Behavior
    private func startTimer() {
        let _ = NSTimer.scheduledTimerWithTimeInterval(0.4, target: self, selector: #selector(moveToNextView), userInfo: nil, repeats: false)
    }

    private func animateLogo() {
        let toValue = logo.layer.position.y + view.frame.height + logo.bounds.height
        
        let animation = CABasicAnimation(keyPath: "position.y").then {
            $0.fromValue = logo.layer.position.y
            $0.toValue = toValue
            $0.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            $0.removedOnCompletion = true
            $0.duration = 1
        }
        
        logo?.layer.addAnimation(animation, forKey: nil)
        logo?.layer.position.y = toValue
    }
    
    private func animateBackground() {
        let enterAnimation = CABasicAnimation(keyPath: "opacity").then {
            $0.fromValue = 0.0
            $0.toValue = 1.0
            $0.duration = 1.0
            $0.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
            $0.beginTime = CACurrentMediaTime() + 0.7
            $0.fillMode = kCAFillModeBackwards
            $0.removedOnCompletion = true
        }
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.startTimer()
        }
        
        contentView?.layer.addAnimation(enterAnimation, forKey: nil)
        contentView?.layer.opacity = 1
        
        enterAnimation.beginTime = CACurrentMediaTime() + 0.45
        
        mainTitle?.layer.addAnimation(enterAnimation, forKey: nil)
        mainTitle?.layer.opacity = 1
        
        CATransaction.commit()
    }
    
    //There is no logic at this view so there is no need for a complex architecture
    func moveToNextView() {
        let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let router = appDelegate.container.resolve(MainRouter.self)
        router?.showRequestInput()
    }

}
